#include <iom64.h>
#include <intrinsics.h>

#define E PORTF4
#define RW PORTF5
#define RS PORTF6
#define F_CPU 16000000

unsigned char str []={"��������"};
unsigned char text []={"AbCdE123445"};

void init(void);
void LCD_write_command(unsigned char data);
void LCD_write_data(unsigned char data);
void LCD_SendString(unsigned char *str);
void LCD_write_data(unsigned char data);
void mesto(unsigned char X, unsigned char Y);
void LCD_proverka_rus(unsigned char data);
unsigned char a[66] = {
0x41, 0xA0, 0x42, 0xA1, 0xE0, 0x45, 0xA3, 0xA4, 0xA5, 0xA6, 0x4B, 
0xA7, 0x4D, 0x48, 0x4F, 0xA8, 0x50, 0x43, 0x54, 0xA9, 0xAA, 0x58, 
0xE1, 0xAB, 0xAC, 0xE2, 0xAD, 0xAE, 0x62, 0xAF, 0xB0, 0xB1, 
0x61, 0xB2, 0xB3, 0xB4, 0xE3, 0x65, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA,
0xBB, 0xBC, 0xBD, 0x6F, 0xBE, 0x70, 0x63, 0xBF, 0x79, 0xE4, 0x78, 
0xE5, 0xC0, 0xC1, 0xE6, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xA2, 0xB5};




void main( void )
{

  DDRF=0x7F;                                                //��������� ������ 0-6 �� ����� ������
  DDRB |= (1<<5);                                           //��������� ����� B5 �� ����� ������
  PORTF=0x00;                                               //��������� �������� ����� F
  init();
  mesto(0, 1);
  LCD_SendString(str);                                      //�������� ������
  __delay_cycles(F_CPU*2.0);
  //LCD_write_command(0x01); 
  mesto(0, 2);  
  LCD_SendString(text);                                     //�������� ������
  while(1)
  {
    __delay_cycles(F_CPU*0.5);
    PORTB ^= (1<<5);                                        //������� ��������
  }
  
}


void LCD_write_command(unsigned char data)
{
  unsigned char tmp;
  PORTF &= ~(1<<RS);                                        //��������� RS
  tmp = data;                                               //���������� ������ � �����������
  tmp = tmp>>4;                                             //����� ����� �� 4  
  PORTF &= 0xF0;                                            //��������� F0-F3
  PORTF |= tmp;                                             //������ ������� �������
  
  PORTF |= (1<<E);  
  __delay_cycles(F_CPU*0.001);
  PORTF &=~(1<<E);   
  
  __delay_cycles(F_CPU*0.001);
  tmp = data;
  tmp &= 0x0F;
  PORTF &= 0xF0;
  PORTF |= tmp;                                             //�������� ������� ������� 
  
  PORTF |= (1<<E);
  __delay_cycles(F_CPU*0.001);
  PORTF &= ~(1<<E);
  
  __delay_cycles(F_CPU*0.001);
}


void init(void)
{
  __delay_cycles(F_CPU*0.015);  
  PORTF &= ~(1<<RW);
  PORTF = 0x03;  
    PORTF |= (1<<E);
  __delay_cycles(F_CPU*0.001);
  PORTF &=~(1<<E);
   
  __delay_cycles(F_CPU*0.0041);  
      PORTF |= (1<<E);
  __delay_cycles(F_CPU*0.001);
  PORTF &=~(1<<E);
  
   __delay_cycles(F_CPU*0.0001); 
      PORTF |= (1<<E);
  __delay_cycles(F_CPU*0.001);
  PORTF &=~(1<<E);
   
  PORTF = 0x02;
  __delay_cycles(F_CPU*0.001);  
  PORTF |= (1<<E);
  __delay_cycles(F_CPU*0.001);
  PORTF &=~(1<<E);                                          //��������� LCD �� ������ �� 4 ������ ���� 
  
  LCD_write_command(0x28);
  LCD_write_command(0x28);
  LCD_write_command(0x28);  
  LCD_write_command(0x0E);                                  //����� ������ ����������� D=1, C=1, B=0
  LCD_write_command(0x01);                                  //������� ������
  LCD_write_command(0x06);                                  //����������� ������ ������� ��� ������ I/D=1 S=0 
  LCD_write_command(0x85);  
}


void LCD_write_data(unsigned char data)
{
  unsigned char tmp; 
  PORTF |= (1<<RS);                                         //��������� ������� RS=1
  tmp = data;         
  tmp = tmp>>4;
  PORTF &= 0xF0;
  PORTF |= tmp;                                             //�������� ������� ������� 
  
  PORTF |= (1<<E);
  __delay_cycles(F_CPU*0.001);
  PORTF &= ~(1<<E);

  tmp = data;
  tmp &= 0x0F;
  PORTF &= 0xF0;
  PORTF |= tmp;                                             //�������� ������� �������
  
  PORTF |= (1<<E);
  __delay_cycles(F_CPU*0.001);
  PORTF &= ~(1<<E);
  
  __delay_cycles(F_CPU*0.001);
}

void LCD_SendString(unsigned char *str)
{
  while(*str)
  {                                                         //�������� ������� �� ������
  LCD_proverka_rus(*str++);
  }
}

void mesto(unsigned char X, unsigned char Y)
{
    unsigned char tmp;
    tmp = 0x00;
    if(Y==1)
    {
      tmp=tmp+X+0x80;                                       //���������� ������� ����� ������� � LCD (1 ������)
    }
    else
    {
      tmp=tmp+X+0x40+0x80;                                  //���������� ������� ����� ������� � LCD (2 ������)
    }
   LCD_write_command(tmp);
}

void LCD_proverka_rus(unsigned char data)
{
  unsigned char buki;
  unsigned char otp;
  otp=0;
  buki=0;
  buki=data;
  if(buki > 0xBF)                                           //�������� ��� ����� �������
  {
    if(buki > 0xDF)                                         //�������� �� ������� 
    {
      if(buki>>4 == 0x0F)
      {buki = buki & 0x0F | 0x30;
      otp = a[buki];}
      else
      {buki = buki & 0x0F | 0x20;
      otp = a[buki];}
    }
    else
    {
      if(buki>>4 == 0x0D)                                   //�������� �� ������ 16 ���� �������� ���������
      {buki = buki & 0x0F | 0x10;                           //���� ����� ������ 16 �� ����������� 16 
       otp = a[buki];}                                      //���������� ���� ����� �� �������
      else
      {buki = buki & 0x0F;                                  //��������� ����� �� ����� ����� � �������
      otp = a[buki];}                                       //���������� ���� ����� �� �������
    }
  }
  else
  {
    if(buki == 0xA8)                                        //�������� �� �
     otp = a[64];
    else
    {
      if(buki == 0xB8)                                      //�������� �� �
       otp = a[65];
      else
       otp = buki;  
    }
  }

  LCD_write_data(otp);                                      //�������� �����
}


